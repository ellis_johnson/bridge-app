using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using BridgeApp.Core.ViewModels;

namespace BridgeApp.Controls
{
    public class CustomImageView : ImageView
    {
        public CustomImageView(Context context) : base(context) {

        }
        public CustomImageView(Context context, IAttributeSet attrs) : base(context)
        {

        }

        public CustomImageView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context)
        {

        }

        public CustomImageView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context)
        {

        }
        private Vehicle _vehicle;
        public Vehicle theVehicle
        {
            get { return _vehicle; }
            set {
                    _vehicle = value;
                    AdjustImage();
                }
        }

        private void AdjustImage()
        {
            switch (theVehicle)
            {
                case (Vehicle.Sedan):
                    SetImageResource(Resource.Drawable.Sedan);
                    break;
                case (Vehicle.SUV):
                    SetImageResource(Resource.Drawable.SUV);
                    break;
                case (Vehicle.Van):
                    SetImageResource(Resource.Drawable.Van);
                    break;      
                case (Vehicle.Small_Truck):
                    SetImageResource(Resource.Drawable.SmallTruck);
                    break;
                case (Vehicle.Bus):
                    SetImageResource(Resource.Drawable.Bus);
                    break;
                case (Vehicle.Semi_Trailer):
                    SetImageResource(Resource.Drawable.Semitrailer);
                    break;
            }
        }
       
    }
}