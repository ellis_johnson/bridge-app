using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using BridgeApp.Core.ViewModels;
using MvvmCross.Droid.Views;
using Android.Preferences;
using MvvmCross.Core.ViewModels;
using System.Collections.Generic;
using Android.Provider;
using Android.Content.PM;
using Java.IO;
using Android.Graphics;
using ZXing.Mobile;
using static Android.Widget.SeekBar;
using BridgeApp.Controls;
using MvvmCross.Binding.Droid.Views;

namespace BridgeApp.Droid.Views
{
    [MvxViewFor(typeof(VehicleInfoViewModel))]
    [Activity(Label = "Vehicle Information", Icon = "@drawable/Icon" , ScreenOrientation = ScreenOrientation.Portrait)]
    public class VehicleInfoView : MvxActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.VehicleInfo);
            
            Button OK = FindViewById<Button>(Resource.Id.btnConfirm);
            OK.Click += (s, e) =>
            {
                var intent = new Intent(this, typeof(MapView));
                this.StartActivity(intent);
            };

            //Setup QR scanner
            if (IsThereAnAppToTakePictures())
            {
                Button buttonScan = FindViewById<Button>(Resource.Id.btnScan);
                double height;
                string output;
                string vehicle;
                string heightString;
                buttonScan.Click += async (sender, e) =>
                {
                    // Initialize the scanner first so it can track the current context
                    MobileBarcodeScanner.Initialize(Application);
                    var scanner = new MobileBarcodeScanner();
                    var result = await scanner.Scan();

                    if (result != null)
                    {
                        output = result.Text;
                        vehicle = output.Substring(0, 1);
                        heightString = output.Substring(1, 3);
                        
                        if (double.TryParse(heightString, out height))
                        {
                            var intent = new Intent(this, typeof(MapView));
                            GlobalVariables.Height = height;
                            GlobalVariables.vehicle = (Vehicle)int.Parse(vehicle);

                            //Save Global variables so they can be re-usd
                            switch (GlobalVariables.vehicle)
                            {
                                case (Vehicle.Sedan):
                                    GlobalVariables.sliderValue = (height - 1.25) / 1 * 100;
                                    break;
                                case (Vehicle.SUV):
                                    GlobalVariables.sliderValue = (height - 1.5) / 1 * 100;
                                    break;
                                case (Vehicle.Van):
                                    GlobalVariables.sliderValue = (height - 2) / 1 * 100;
                                    break;
                                case (Vehicle.Small_Truck):
                                    GlobalVariables.sliderValue = (height - 2.5) / 1 * 100;
                                    break;
                                case (Vehicle.Bus):
                                    GlobalVariables.sliderValue = (height - 2.5) / 2 * 100;
                                    break;
                                case (Vehicle.Semi_Trailer):
                                    GlobalVariables.sliderValue = (height - 3) / 2 * 100;
                                    break;
                            }
                            this.StartActivity(intent);
                        }
                        else
                        {
                            var toastText = "";
                            toastText = "Invalid Code Scanned";
                            Context context = ApplicationContext;
                            Toast toast = Toast.MakeText(context, toastText, ToastLength.Short);
                        }
                    }
                };
            }
        }

        private bool IsThereAnAppToTakePictures()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities =
            PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }
    }
}