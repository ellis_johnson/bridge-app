using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using BridgeApp.Core.Models;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Gms.Maps.Model;
using Android.Gms.Maps;
using Android.Views;
using Android.Locations;
using Android.Content;
using Android.Graphics;
using MvvmCross.Droid.Views;
using BridgeApp.Core.ViewModels;
using Android.Gms.Common.Apis;
using Android.Media;
using Android.Net;

namespace BridgeApp.Droid.Views
{
    [Activity(Label = "Map", Icon = "@drawable/Icon")]
    public class MapView : MvxActivity, IOnMapReadyCallback, ILocationListener, GoogleMap.IOnMyLocationButtonClickListener
    {
        private delegate IOnMapReadyCallback OnMapReadyCallback();
        LocationManager locationManager;
        MapViewModel vm;
        AlertDialog alert;
        private double height;
        GoogleMap googleMap;
        List<string> seenBridges;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Map);
            seenBridges = new List<string>();
            locationManager = GetSystemService(Context.LocationService) as LocationManager;
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar); 
            height = GlobalVariables.Height;
            ActionBar.Title = string.Format("Map at Height: {0}m", height);
            if (!locationManager.IsProviderEnabled(LocationManager.GpsProvider) && GlobalVariables.AskLocation)
            {
                CheckLocationSettings();
            }
            vm = ViewModel as MapViewModel;
            MapFragment mapFrag = FragmentManager.FindFragmentById<MapFragment>(Resource.Id.map);
            mapFrag.GetMapAsync(this);
        }

        protected override void OnResume()
        {
            base.OnResume();
            seenBridges = new List<string>();
            if (locationManager.IsProviderEnabled(LocationManager.GpsProvider))
            {
                locationManager.RequestLocationUpdates(LocationManager.GpsProvider, 3000, 50, this);
            }
        }

        protected override void OnPause()
        {
            base.OnPause();
            locationManager.RemoveUpdates(this);
        }
        
        public bool OnMyLocationButtonClick()
        {
            if (locationManager.IsProviderEnabled(LocationManager.GpsProvider))
            {
                var location = GetLastKnownLocation();
                if (location != null)
                {
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                    .Target(new LatLng(location.Latitude, location.Longitude)).Zoom(17.0f).Build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
                    googleMap.AnimateCamera(cameraUpdate);
                    GlobalVariables.Following = !GlobalVariables.Following;
                    Toast.MakeText(this,
                        "Following: " + GlobalVariables.Following.ToString(),
                        ToastLength.Short).Show();
                    return true;
                }
                Toast.MakeText(this,
                        "Cannot determine location, please try again later.",
                        ToastLength.Short).Show();
                return true;
            }
            
            Toast.MakeText(this,
            "Location Services Disabled",
            ToastLength.Short).Show();
            return true;
            
        }

        private Location GetLastKnownLocation()
        {
            List<String> providers = locationManager.GetProviders(true).ToList();
            Location bestLocation = null;
            foreach (var provider in providers)
            {
                Location l = locationManager.GetLastKnownLocation(provider);
                if (l == null)
                {
                    continue;
                }
                if (bestLocation == null || l.Accuracy < bestLocation.Accuracy)
                {
                    bestLocation = l;
                }
            }
            return bestLocation;
        }

        public void OnLocationChanged (Android.Locations.Location location)
        {
            if (GlobalVariables.Following)
            {
                CameraPosition cameraPosition = new CameraPosition.Builder()
                    .Target(new LatLng(location.Latitude, location.Longitude))
                    .Zoom(17.0f).Build();
                CameraUpdate cameraUpdate = CameraUpdateFactory
                    .NewCameraPosition(cameraPosition);
                googleMap.AnimateCamera(cameraUpdate);
            }

            foreach (var bridge in vm.DangerBridges)
            {
                //Check if user has been alerted
                if (!seenBridges.Any(s => bridge.ASSET_ID.Contains(s)))
                {
                    //Warn about bridges less than 200m
                    if (getDistance(location, bridge) < 200)
                    {
                        seenBridges.Add(bridge.ASSET_ID);
                        Toast.MakeText(this,
                            "Avoid: " + bridge.Street_Name + ", " + bridge.Direction,
                            ToastLength.Long).Show();
                        Android.Net.Uri notification = RingtoneManager.GetDefaultUri(RingtoneType.Notification);
                        Ringtone notificationSound = RingtoneManager.GetRingtone(this, notification);
                        notificationSound.Play();
                    }
                }
            }
        }

        public void CheckLocationSettings()
        {
            //Avoid asking user multiple times
            GlobalVariables.AskLocation = false;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle("Enable Location Services")
                .SetMessage("In order to display your position on the map, you will need to enable location services on your device.")
                .SetNegativeButton("Dismiss", IgnoreLocation)
                .SetPositiveButton("Enable", EnableLocation);
            alert = builder.Create();
            alert.Show();
        }

        private void EnableLocation(object sender, DialogClickEventArgs e)
        {
            //Take the user to their location service
            var intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
            StartActivity(intent);
        }

        private void IgnoreLocation(object sender, DialogClickEventArgs e)
        {
            alert.Dismiss();
        }

        public async void OnMapReady(GoogleMap map)
        {
            googleMap = map;
            map.SetOnMyLocationButtonClickListener(this);
            //Set Default View to Brisbane
            MapsInitializer.Initialize(this);
            int zoom = 15;
            LatLng location = new LatLng(-27.4698, 153.0251);
            if (vm.custom_view)
            {
                location = new LatLng(vm.DangerBridges.First().Latitude, vm.DangerBridges.First().Longitude);
                zoom = 16;
            }
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(location)
                .Zoom(zoom);
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            
            //Add dangerous hazards
            foreach (var bridge in vm.DangerBridges)
            {
                MarkerOptions markerOpt = new MarkerOptions()
                    .SetPosition(new LatLng(bridge.Latitude, bridge.Longitude))
                    .SetTitle(bridge.Signed_Clearance + "m, " + bridge.Street_Name + ", " + bridge.Suburb)
                    .SetSnippet(bridge.Description)
                    .SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.warning_red));
                map.AddMarker(markerOpt);
            }

            //Add possible hazards
            foreach (var bridge in vm.WarningBridges)
            {
                MarkerOptions markerOpt = new MarkerOptions()
                    .SetPosition(new LatLng(bridge.Latitude, bridge.Longitude))
                    .SetTitle(bridge.Signed_Clearance + "m, " + bridge.Street_Name + ", " + bridge.Suburb)
                    .SetSnippet(bridge.Description)
                    .SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.warning_yellow));
                map.AddMarker(markerOpt);
            }
            
            map.UiSettings.TiltGesturesEnabled = false;
            map.UiSettings.MapToolbarEnabled = false;
            map.MyLocationEnabled = true;
            map.UiSettings.MyLocationButtonEnabled = true;
            map.MoveCamera(cameraUpdate);
            
            if (GlobalVariables.DisplayRoute)
            {
                GlobalVariables.DisplayRoute = false;
                if (IsNetworkAvailable())
                {
                    vm.RouteDirections = await vm.GetDirections();
                    if (vm.RouteDirections != null && vm.RouteDirections.routes.Count > 0)
                    {
                        List<Color> Colours = new List<Color> { Color.CornflowerBlue, Color.LightGoldenrodYellow, Color.LawnGreen, Color.MediumVioletRed };
                        for (int r = vm.RouteDirections.routes.Count - 1; r >= 0; r--)
                        {
                            var route = vm.RouteDirections.routes[r];
                            PolylineOptions outline = new PolylineOptions().Clickable(false).InvokeWidth(20).InvokeColor(Color.Black);
                            PolylineOptions polyline = new PolylineOptions().Clickable(true).InvokeWidth(15).InvokeColor(Colours.ElementAt(r));

                            var points = DecodePolylinePoints(route.overview_polyline.points);
                            foreach (var p in points)
                            {
                                polyline.Add(p);
                                outline.Add(p);
                            }

                            //Add route info marker
                            var routeinfo = route.legs.First();
                            var descText = String.Format("Estimated Time: {0}, Distance: {1}", routeinfo.duration.text, routeinfo.distance.text);
                            var titleText = String.Format("Possible route via {0}", route.summary);
                            int mid = (points.Count / 2);
                            MarkerOptions markerOpt = new MarkerOptions()
                                .SetPosition(points.ElementAt(mid))
                                .SetTitle(titleText)
                                .SetSnippet(descText)
                                .SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.warning_blue));
                            map.AddMarker(markerOpt);

                            map.AddPolyline(outline);
                            var line = map.AddPolyline(polyline);
                        }
                        var south = vm.RouteDirections.routes.First().bounds.southwest.lng;
                        var west = vm.RouteDirections.routes.First().bounds.southwest.lat;
                        var north = vm.RouteDirections.routes.First().bounds.northeast.lng;
                        var east = vm.RouteDirections.routes.First().bounds.northeast.lat;
                        foreach (var route in vm.RouteDirections.routes)
                        {
                            if (route.bounds.southwest.lng < south)
                            {
                                south = route.bounds.southwest.lng;
                            }
                            if (route.bounds.southwest.lat < west)
                            {
                                west = route.bounds.southwest.lat;
                            }
                            if (route.bounds.northeast.lng > north)
                            {
                                north = route.bounds.northeast.lng;
                            }
                            if (route.bounds.northeast.lng > east)
                            {
                                east = route.bounds.northeast.lat;
                            }
                        }
                        //Add flags
                        MarkerOptions startFlag = new MarkerOptions()
                            .SetPosition(new LatLng(vm.RouteDirections.routes.First().legs.First().start_location.lat, vm.RouteDirections.routes.First().legs.First().start_location.lng))
                            .SetTitle(GlobalVariables.StartLocation)
                            .SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.finish_flag));
                        MarkerOptions endFlag = new MarkerOptions()
                            .SetPosition(new LatLng(vm.RouteDirections.routes.First().legs.Last().end_location.lat, vm.RouteDirections.routes.First().legs.Last().end_location.lng))
                            .SetTitle(GlobalVariables.EndLocation)
                            .SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.finish_flag));
                        map.AddMarker(startFlag);
                        map.AddMarker(endFlag);
                        map.MoveCamera(CameraUpdateFactory.NewLatLngBounds(new LatLngBounds(new LatLng(west, south), new LatLng(east, north)), 10));
                    }
                    else
                    {
                        Toast.MakeText(this, "Could not establish a route between the two given addresses. Ensure that the address are correctly formatted.", ToastLength.Long).Show();
                    }
                }
                else
                {
                    Toast.MakeText(this, "Cannot connect to internet. Please try again later.", ToastLength.Long).Show();
                }
            }
        }

        private bool IsNetworkAvailable()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(Context.ConnectivityService);
            NetworkInfo activeNetworkInfo = connectivityManager.ActiveNetworkInfo;
            return activeNetworkInfo != null && activeNetworkInfo.IsConnected;
        }

        //turn the encodedpoint stirng into list of latlng
        private List<LatLng> DecodePolylinePoints(string encodedPoints)
        {
            if (encodedPoints == null || encodedPoints == "") return null;
            List<LatLng> poly = new List<LatLng>();
            char[] polylinechars = encodedPoints.ToCharArray();
            int index = 0;
            int currentLat = 0;
            int currentLng = 0;
            int next5bits;
            int sum;
            int shifter;
            try
            {
                while (index < polylinechars.Length)
                {
                    // calculate next latitude
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylinechars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylinechars.Length);
                    if (index >= polylinechars.Length)
                        break;
                    currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);
                    //calculate next longitude
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylinechars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylinechars.Length);
                    if (index >= polylinechars.Length && next5bits >= 32)
                        break;
                    currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);
                    LatLng p = new LatLng(Convert.ToDouble(currentLat) / 100000.0,
                        Convert.ToDouble(currentLng) / 100000.0);
                    poly.Add(p);
                }
            }
            catch (Exception e)
            {
                //log
            }
            return poly;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            //Back Button
            if (item.ItemId == Android.Resource.Id.Home)
            {
                FinishAffinity();
                Intent intent = new Intent(this, typeof(HomeView));
                StartActivity(intent);
                return true;
            }
            return false;
        }

        private double rad(double x)
        {
            return x * Math.PI / 180;
        }

        //return distance in meters
        private double getDistance(Location p1, BridgeModel p2)
        {
            var R = 6378137;
            var dLat = rad(p2.Latitude - p1.Latitude);
            var dLong = rad(p2.Longitude - p1.Longitude);
            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2)
                + Math.Cos(rad(p1.Latitude)) * Math.Cos(rad(p2.Latitude))
                * Math.Sin(dLong / 2) * Math.Sin(dLong / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c;
            return d;
        }

        public void OnProviderEnabled(string provider)
        {
            //when user enables GPS
        }

        public void OnProviderDisabled(string provider)
        {
            //when user disables GPS
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            //when avaliability changes
        }
    }
}