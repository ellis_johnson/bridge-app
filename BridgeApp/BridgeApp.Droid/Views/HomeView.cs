using System;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using MvvmCross.Droid.Views;
using MvvmCross.Core.ViewModels;
using BridgeApp.Core.ViewModels;

namespace BridgeApp.Droid.Views
{
    [MvxViewFor(typeof(HomeViewModel))]
    [Activity(Label = "BridgeApp", Icon = "@drawable/Icon")]
    public class HomeView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Home);
            ImageButton addInfo = FindViewById<ImageButton>(Resource.Id.addInfo);
            addInfo.Click += addInfo_Click;
        }

        void addInfo_Click(object sender, EventArgs e)
        {
            Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
            Android.App.AlertDialog alertDialog = builder.Create();
            alertDialog.SetTitle("Additional Information");
            alertDialog.SetMessage("Contact No:\n12345678\n\n"
                + "Email Address:\nDreamlandDwellers@email.com\n\n"
                + "Made By:\nDaniel, Ellis and Minh\n\n"
                + "Icons and Images:\nicons8.com");
            alertDialog.SetIcon(Resource.Drawable.brisbanecitycouncil);
            alertDialog.SetButton("Ok", (s, ev) =>
            {
                //do something
            });

            alertDialog.Show();
        }
    }
}
