using System;
using System.IO;
using Android.App;
using Android.Gms.Maps;
using MvvmCross.Droid.Views;
using Android.OS;
using Android.Widget;
using Android.Content;
using BridgeApp.Core.ViewModels;

namespace BridgeApp.Droid.Views
{
    [Activity(Label = "Navigation", Icon = "@drawable/Icon")]
    public class NavigationView : MvxActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Navigation);
            Button OK = FindViewById<Button>(Resource.Id.btnConfirm);
            OK.Click += (s, e) =>
            {
                var vm = ViewModel as NavigationViewModel;
                GlobalVariables.StartLocation = vm.InputStartLocation;
                GlobalVariables.EndLocation = vm.InputEndLocation;
                GlobalVariables.DisplayRoute = true;

                var intent = new Intent(this, typeof(MapView));
                this.StartActivity(intent);
            };
        }
    }
}