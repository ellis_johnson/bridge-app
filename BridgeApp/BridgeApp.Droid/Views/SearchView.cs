using Android.App;
using Android.OS;
using Android.Widget;
using BridgeApp.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using Android.Views;

namespace BridgeApp.Droid.Views
{
    [MvxViewFor(typeof(SearchViewModel))]
    [Activity(Label = "Search", Icon = "@drawable/Icon")]
    public class SearchView : MvxActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Search);
        }
    }
}