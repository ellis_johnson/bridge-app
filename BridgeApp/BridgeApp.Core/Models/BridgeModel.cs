using System;
using Newtonsoft.Json;

namespace BridgeApp
{
    public class BridgeModel
    {
        public string ASSET_ID { get; set; }
        [JsonProperty("Angle 1")]
        public float Angle_1 { get; set; }
        [JsonProperty("Angle 2")]
        public float Angle_2 { get; set; }
        public string COUNTER { get; set; }
        public string Description { get; set; }
        public string Direction { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public double Signed_Clearance { get; set; }
        public double SIGNED_CLEARANCE_MAX { get; set; }
        public string Street_Name { get; set; }
        public string Structure_ID { get; set; }
        public string Suburb { get; set; }
    }
}