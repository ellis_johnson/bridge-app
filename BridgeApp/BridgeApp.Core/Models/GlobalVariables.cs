﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgeApp.Core.ViewModels
{
    public static class GlobalVariables
    {
        public static double Height = 4.6;
        public static Vehicle vehicle = Vehicle.Sedan;
        public static double sliderValue = 0;
        public static bool AskLocation = true;
        public static bool Following = false;
        public static bool DisplayRoute = false;
        public static string StartLocation = "Roma Street, Brisbane, QLD";
        public static string EndLocation = "Queen Street, Brisbane, QLD";
        public static BridgeModel BridgeOfInterst = null;
        public static string DirectionsAPIKey = "AIzaSyBou1CAo6SBKPENu-FO8SHbMHbePxgbn9g";
    }
}
