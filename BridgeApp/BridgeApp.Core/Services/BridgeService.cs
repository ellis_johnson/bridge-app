﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using BridgeApp.Core.ViewModels;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BridgeApp.Core.Services
{
    class BridgeService
    {
        public List<BridgeModel> Bridges { get; }

        public BridgeService()
        {
            var assembly = typeof(BridgeService).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream("BridgeApp.Core.bridges.json");
            using (StreamReader sr = new StreamReader(stream))
            {
                JsonSerializer serializer = new JsonSerializer();
                Bridges = JsonConvert.DeserializeObject<List<BridgeModel>>(sr.ReadToEnd());
            }
        }
    }
}
