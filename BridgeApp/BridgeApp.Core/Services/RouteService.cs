﻿using Android.Locations;
using BridgeApp.Core.Models;
using BridgeApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BridgeApp.Core.Services
{
    class RouteService
    {
        public async Task<Directions.RootObject> GetDirections()
        {
            if (GlobalVariables.StartLocation == "" || GlobalVariables.EndLocation == "")
            {
                return null;
            }

            WebRequest request =
                WebRequest.CreateHttp(String.Format(
                    "https://maps.googleapis.com/maps/api/directions/json?origin={0}&destination={1}&alternatives=true&key={2}",
                    GlobalVariables.StartLocation, 
                    GlobalVariables.EndLocation, 
                    GlobalVariables.DirectionsAPIKey));
            string responseValue = null;
            using (var response = await request.GetResponseAsync())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream != null)
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            responseValue = await reader.ReadToEndAsync();
                        }
                    }
                }
            }
            
            Directions.RootObject sresponse;
            try
            {
                sresponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Directions.RootObject>(responseValue);
            } catch
            {
                return null;
            }
            if (sresponse != null)
            {
                return sresponse;
            } else
            {
                return null;
            }
            
        }        
    }
}
