using MvvmCross.Core.ViewModels;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BridgeApp.Core.ViewModels
{

    public enum Vehicle
    {
        Sedan = 0,
        SUV = 1,
        Van = 2,
        [EnumMember(Value = "Small Truck")]
        Small_Truck = 3,
        Bus = 4,
        [EnumMember(Value = "Semi-Trailer")]
        Semi_Trailer = 5
    }

    public class VehicleInfoViewModel
        : MvxViewModel
    {
        
        public VehicleInfoViewModel()
        {
            Total = GlobalVariables.Height;
            VehicleEnum = GlobalVariables.vehicle;
            sliderValue = GlobalVariables.sliderValue;

        }
        private Vehicle _vehicle;
        public Vehicle VehicleEnum
        {
            get { return _vehicle; }
            set
            {
                _vehicle = value;
                GlobalVariables.vehicle = _vehicle;
                switch (VehicleEnum)
                {
                    case (Vehicle.Sedan):
                        Height = 1.25;
                        Multiplier = 1;
                        break;

                    case (Vehicle.SUV):
                        Height = 1.5;
                        Multiplier = 1;
                        break;

                    case (Vehicle.Van):
                        Height = 2;
                        Multiplier = 1;
                        break;

                    case (Vehicle.Small_Truck):
                        Height = 2.5;
                        Multiplier = 1;
                        break;

                    case (Vehicle.Bus):
                        Height = 2.5;
                        Multiplier = 2;
                        break;

                    case (Vehicle.Semi_Trailer):
                        Height = 3;
                        Multiplier = 2;
                        break;
                }
                Total = Height + (sliderValue / 100 * Multiplier);
                RaiseAllPropertiesChanged();
            }
        }

        private double _sliderValue = 0;
        public double sliderValue
        {
            get { return _sliderValue; }
            set
            {
                _sliderValue = value;
                GlobalVariables.sliderValue = _sliderValue;
                switch (VehicleEnum)
                {
                    case (Vehicle.Sedan):
                        Height = 1.25;
                        Multiplier = 1;
                        break;

                    case (Vehicle.SUV):
                        Height = 1.5;
                        Multiplier = 1;
                        break;

                    case (Vehicle.Van):
                        Height = 2;
                        Multiplier = 1;
                        break;

                    case (Vehicle.Small_Truck):
                        Height = 2.5;
                        Multiplier = 1;
                        break;

                    case (Vehicle.Bus):
                        Height = 2.5;
                        Multiplier = 2;
                        break;

                    case (Vehicle.Semi_Trailer):
                        Height = 3;
                        Multiplier = 2;
                        break;
                }
                Total = Height + (sliderValue / 100 * Multiplier);
                RaiseAllPropertiesChanged();
            }
        }

        private List<Vehicle> _list = new List<Vehicle>()
        {
            Vehicle.Sedan, Vehicle.SUV, Vehicle.Van, Vehicle.Small_Truck, Vehicle.Bus, Vehicle.Semi_Trailer
        };

        public List<Vehicle> list
        {
            get { return _list; }
            set { _list = value; RaiseAllPropertiesChanged(); }
        }

        private double _height = 1.25;
        public double Height
        {
            get { return _height; }
            set
            {
                _height = value;
                RaisePropertyChanged(() => Height);
            }
        }
        private int _multiplier;
        public int Multiplier
        {
            get { return _multiplier; }
            set
            {
                _multiplier = value;
                RaisePropertyChanged(() => Multiplier);
            }
        }

        private double _total;
        public double Total
        {
            get { return _total; }
            set {
                _total = value;
                GlobalVariables.Height = _total;
                TotalString = string.Format("{0}m", _total);
                RaisePropertyChanged(() => Total);
                RaisePropertyChanged(() => TotalString);
            }
        }
        private string _totalString = "1.25m";
        public string TotalString
        {
            get { return _totalString; }
            set
            {
                _totalString = value;
                RaisePropertyChanged(() => TotalString);
            }
        }


    }
}
