using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using MvvmCross.Core.ViewModels;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using PCLStorage;
using System.Reflection;
using BridgeApp.Core.Services;
using System;
using BridgeApp.Core.Models;

namespace BridgeApp.Core.ViewModels
{
    public class MapViewModel : MvxViewModel
    {
        public bool custom_view;
        public Directions.RootObject RouteDirections;
        private List<BridgeModel> dangerBridges;
        private List<BridgeModel> warningBridges;

        public List<BridgeModel> DangerBridges
        {
            get { return dangerBridges; }
        }

        public List<BridgeModel> WarningBridges
        {
            get { return warningBridges; }
        }

        public void Search()
        {
            ShowViewModel<NavigationViewModel>();
        }

        public override void Start()
        {
            base.Start();

            if (GlobalVariables.BridgeOfInterst != null)
            {
                custom_view = true;
                dangerBridges = new List<BridgeModel>();
                warningBridges = new List<BridgeModel>();
                dangerBridges.Add(GlobalVariables.BridgeOfInterst);
                GlobalVariables.BridgeOfInterst = null;
            } else
            {
                custom_view = false;
                List<BridgeModel> AllBridges = new BridgeService().Bridges;
                dangerBridges = new List<BridgeModel>();
                warningBridges = new List<BridgeModel>();
                foreach (var b in AllBridges)
                {
                    if (b.Signed_Clearance <= GlobalVariables.Height)
                    {
                        dangerBridges.Add(b);
                    }
                }
                foreach (var b in AllBridges)
                {
                    if (b.Signed_Clearance > GlobalVariables.Height && b.Signed_Clearance < GlobalVariables.Height + 0.3)
                    {
                        warningBridges.Add(b);
                    }
                }
            }
        }

        public async Task<Directions.RootObject> GetDirections()
        {
            var routeService = new RouteService();
            return await routeService.GetDirections();
        }
    }
}