using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using MvvmCross.Core.ViewModels;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using PCLStorage;
using System.Reflection;
using BridgeApp.Core.Services;
using System;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Android.Locations;

namespace BridgeApp.Core.ViewModels
{
    public class NavigationViewModel : MvxViewModel
    {
        private string _InputStartLocation = GlobalVariables.StartLocation;
        public string InputStartLocation
        {
            get { return _InputStartLocation; }
            set
            {
                _InputStartLocation = value;
                RaiseAllPropertiesChanged();
            }
        }

        private string _InputEndLocation = GlobalVariables.EndLocation;
        public string InputEndLocation
        {
            get { return _InputEndLocation; }
            set
            {
                _InputEndLocation = value;
                RaiseAllPropertiesChanged();
            }
        }
    }
}