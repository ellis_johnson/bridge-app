﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using System.IO;
using BridgeApp.Core.Services;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace BridgeApp.Core.ViewModels
{
    public class SearchViewModel
        : MvxViewModel
    {
        public SearchViewModel()
        {
            suburbs = new ObservableCollection<BridgeModel>();
            SearchLocations();
            suburbs = new ObservableCollection<BridgeModel>((suburbs.OrderBy(a => a.Suburb)));
            SelectLocationCommand = new MvxCommand<BridgeModel>(selectedLocation => ShowViewModel<SearchViewModel2>(selectedLocation));
        }

        public ICommand SelectLocationCommand { get; private set; }

        private ObservableCollection<BridgeModel> _suburbs;
        public ObservableCollection<BridgeModel> suburbs
        {
            get { return _suburbs; }
            set { SetProperty(ref _suburbs, value); }
        }

        public void SearchLocations()
        {
            var newList = new List<string>();
            BridgeService bridgeservice = new BridgeService();
            suburbs.Clear();
            foreach (var bridge in bridgeservice.Bridges)
            {
                if (!newList.Contains(bridge.Suburb))
                {
                    newList.Add(bridge.Suburb);
                    suburbs.Add(bridge);
                }
            }
        }
    }
}
