﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using System.IO;
using BridgeApp.Core.Services;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace BridgeApp.Core.ViewModels
{
    public class SearchViewModel2
        : MvxViewModel
    {
        public BridgeModel selectedLocation { get; private set; }
        public BridgeModel Suburbs;

        public override void Start()
        {
            base.Start();
            streetname = new ObservableCollection<BridgeModel>();
            SearchLocations();
            streetname = new ObservableCollection<BridgeModel>((streetname.OrderBy(a => a.Street_Name)));
            SelectLocationCommand = new MvxCommand<BridgeModel>(selectedLocation => ShowViewModel<SearchViewModel3>(selectedLocation));
        }

        public void Init(BridgeModel selectedLocation)
        {
             Suburbs = selectedLocation;
        }

        public ICommand SelectLocationCommand { get; private set; }

        private ObservableCollection<BridgeModel> _streetname;
        public ObservableCollection<BridgeModel> streetname
        {
            get { return _streetname; }
            set { SetProperty(ref _streetname, value); }
        }

        private string city;

        public string City
        {
            get { return city; }
            set { SetProperty(ref city, value); }
        }

        public void SearchLocations()
        {
            City = Suburbs.Suburb;
            var newList2 = new List<string>();
            BridgeService bridgeservice = new BridgeService();
            foreach (var bridge in bridgeservice.Bridges)
            {
                if (City == bridge.Suburb)
                {
                    streetname.Add(bridge);
                }
            }
        }
    }
}
