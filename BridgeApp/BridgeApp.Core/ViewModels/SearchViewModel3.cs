﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using System.IO;
using BridgeApp.Core.Services;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace BridgeApp.Core.ViewModels
{
    public class SearchViewModel3
        : MvxViewModel
    {
        public BridgeModel selectedLocation { get; private set; }
        public BridgeModel description;

        public override void Start()
        {
            base.Start();
            bridge_description = new ObservableCollection<BridgeModel>();
            SearchLocations();
        }

        public void Init(BridgeModel selectedLocation)
        {
            description = selectedLocation;
        }

        public ICommand ViewOnMap
        {
            get
            {
                GlobalVariables.BridgeOfInterst = bridge_description.First();
                return new MvxCommand<BridgeModel>(bridge_description => ShowViewModel<MapViewModel>(bridge_description));
            }
        }

        private ObservableCollection<BridgeModel> _bridge_description;
        public ObservableCollection<BridgeModel> bridge_description
        {
            get { return _bridge_description; }
            set { SetProperty(ref _bridge_description, value); }
        }

        private string id;

        public string Id
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        public void SearchLocations()
        {
            Id = description.ASSET_ID;
            var newList2 = new List<string>();
            BridgeService bridgeservice = new BridgeService();
            foreach (var bridge in bridgeservice.Bridges)
            {
                if (Id == bridge.ASSET_ID)
                {
                    bridge_description.Add(bridge);
                }
            }
        }
    }
}
