using MvvmCross.Core.ViewModels;
using System.Windows.Input;

namespace BridgeApp.Core.ViewModels
{
    public class HomeViewModel 
        : MvxViewModel
    {
        public ICommand MapCommand
        {
            get
            {
                return new MvxCommand(() => ShowViewModel<MapViewModel>());
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                return new MvxCommand(() => ShowViewModel<SearchViewModel>());
            }
        }

        public ICommand NavCommand
        {
            get
            {
                return new MvxCommand(() => ShowViewModel<NavigationViewModel>());
            }
        }

        public ICommand VehicleInfoCommand
        {
            get
            {
                return new MvxCommand(() => ShowViewModel<VehicleInfoViewModel>());
            }
        }
    }
}
